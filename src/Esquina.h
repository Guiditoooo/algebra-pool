#include "raylib.h"

namespace POOL {
	namespace ESQUINA {
		class Esquina
		{
		public:
			Esquina();
			~Esquina();

			void dibujar();

			Vector2 getCir();
			void setCir(Vector2 cir);
			float getRadio();
			void setRadio(float radio);
			float getMasa();
			void setMasa(float masa);
			Color getColor();
			void setColor(Color color);

		private:
			Vector2 cir;
			float radio;
			float masa;
			Color color;
		};
	}
}