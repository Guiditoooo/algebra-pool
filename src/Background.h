#pragma once
#include "raylib.h"
#include <iostream>

#include "Borde.h"
#include "Hoyo.h"

namespace POOL {
namespace BACKGROUND {

	enum class HoyoUbicacion { ARRIBAIZQ, ARRIBACEN, ARRIBADER, ABAJOIZQ, ABAJOCEN, ABAJODER };

	/*struct Esquina
	{
		Vector2 cir = { 0, 0 };
		float radio = 0;
		float masa = static_cast<float>(INT_MAX);
		Color color = BLUE;
	};
	/*struct Borde
	{
		BordeUbicacion tipo = BordeUbicacion::ARRIBA;
		Rectangle rec = { 0 };

		Esquina *esq[maxEsquinas];
	};
	struct Hoyo
	{
		Vector2 pos = { 0, 0 };
		Color color = GREEN;
	};*/

	extern Texture2D backgroundTexture;

	void init();
	void draw();
}
}