#include "Gameplay.h"

#include <iostream>

#include "Pelota.h"
#include "Palo.h"
#include "Background.h"
#include "CollisionManager.h"

namespace POOL {
namespace GAMEPLAY {
	using namespace PELOTA;
	using namespace PALO;
	using namespace BACKGROUND;

	Vector2 mouse;
	int lisasEmbocadas = 0;
	int ralladasEmbocadas = 0;
	bool gameOver = false;
	
	Sound paloPelotaColSound;
	Sound pelotaPelotaColSound;
	Sound pelotaBordeColSound;

	static bool mousePresionado = false;

	static void meterPelota();
	static void inputs();
	static void hacks();

	static bool algunaPelotaEnMovimiento();

	void init() {

		gameOver = false;

		paloPelotaColSound = LoadSound("res/CueHit.ogg");
		pelotaPelotaColSound = LoadSound("res/BallBall.ogg");
		pelotaBordeColSound = LoadSound("res/BallWall.ogg");

		SetSoundVolume(pelotaBordeColSound, 0.1f);
		mousePresionado = false;

		PELOTA::init();
		PALO::init();
		BACKGROUND::init();
	}

	void update() {

		if (!gameOver)
		{
			PELOTA::update();
			PALO::update();
			COLLISION_MANAGER::collisionManager();
			inputs();
			meterPelota();
		}

		hacks();
		mouse = GetMousePosition();			
	}		

	const short gameOverYOffset = 60;
	void draw() {

		BACKGROUND::draw();
		PELOTA::draw();
		PALO::draw();

		if (gameOver) {
			DrawText("Game over!", GetScreenWidth() / 2 - MeasureText("Game over!", 60) / 2, GetScreenHeight() / 2 - gameOverYOffset / 2, gameOverYOffset, BLACK);
		}
	}

	void deInit()
	{
		PALO::deInit();
		BORDE::deInit();
		HOYO::deInit();

		UnloadSound(paloPelotaColSound);
		UnloadSound(pelotaPelotaColSound);
		UnloadSound(pelotaBordeColSound);

		UnloadTexture(BACKGROUND::backgroundTexture);
	}

	const float minRadio = 0.5f;
	const short xOffsetLisaAdentro = 150;
	const short xOffsetRalladaAdentro = 730;
	const short yPelotaAdentro = 30;
	void meterPelota() {

		for (int i = 0; i < maxPelotas; i++)
		{
			if (pelota[i]->getDesaparicion())
			{
				pelota[i]->setVel({ 0, 0 });
				pelota[i]->setRadio(pelota[i]->getRadio() * animacionDesaparicion);
				pelota[i]->setEnMovimiento(false);
				if (pelota[i]->getRadio() < minRadio)
				{
					pelota[i]->setActivo(false);
					pelota[i]->setDesaparicion(false);
					switch (pelota[i]->getTipo())
					{
					case TipoPelota::BLANCA:

						break;
					case TipoPelota::LISA:
						pelota[i]->setCir({ (float)(xOffsetLisaAdentro + 2 * radioGral * (i - 1) + separacionPelotasDibujado * (i - 1)), yPelotaAdentro });
						pelota[i]->setRadio(radioGral);
						lisasEmbocadas++;
						break;
					case TipoPelota::RALLADA:
						pelota[i]->setCir({ (float)(xOffsetRalladaAdentro + 2 * radioGral * (i - 1 - maxPelotas / 2) + separacionPelotasDibujado * (i - 1 - maxPelotas / 2)), yPelotaAdentro });
						ralladasEmbocadas++;
						pelota[i]->setRadio(radioGral);
						break;
					case TipoPelota::NEGRA:
						gameOver = true;
						break;
					default:
						break;
					}
					std::cout << "Pelota " << i << " embocada." << std::endl;
				}
			}
		}
	}

	void inputs() {

		if (algunaPelotaEnMovimiento())
		{
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			{
				std::cout << "mouse: " << mouse.x << " - " << mouse.y << std::endl;
			}
		}

		if (COLLISION_MANAGER::chequeoPuntoCirculo(mouse, pelota[0]->getCir(), pelota[0]->getRadio()))
		{
			if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && algunaPelotaEnMovimiento())
			{
				mousePresionado = true;
				std::cout << "Mouse Pos: " << mouse.x << " - " << mouse.y << std::endl;

				PALO::palo->setActivo(true);
			}
		}

		if (IsMouseButtonUp(MOUSE_LEFT_BUTTON) && COLLISION_MANAGER::chequeoPuntoCirculo(mouse, pelota[0]->getCir(), pelota[0]->getRadio()) && mousePresionado)
		{
			mousePresionado = false;
			std::cout << "Suelta." << std::endl;

			PALO::palo->setActivo(false);
		}
		else if (IsMouseButtonUp(MOUSE_LEFT_BUTTON) && mousePresionado && !pelota[0]->getEnMovimiento())
		{
			PlaySound(paloPelotaColSound);
			PALO::palo->setActivo(false);
			mousePresionado = false;
			pelota[0]->setEnMovimiento(true);

			pelota[0]->setVel({ (pelota[0]->getCir().x - mouse.x) / disminucionPorcentual, (pelota[0]->getCir().y - mouse.y) / disminucionPorcentual });

			if (pelota[0]->getVel().x > maxVelPelota) pelota[0]->setVel({ maxVelPelota,pelota[0]->getVel().y });
			if (pelota[0]->getVel().y > maxVelPelota) pelota[0]->setVel({ pelota[0]->getVel().x, maxVelPelota });
			if (pelota[0]->getVel().x < -maxVelPelota) pelota[0]->setVel({ -maxVelPelota,pelota[0]->getVel().y });
			if (pelota[0]->getVel().y < -maxVelPelota) pelota[0]->setVel({ pelota[0]->getVel().x, -maxVelPelota });

			std::cout << "Dispara. Vel.x: " << pelota[0]->getVel().x << " - Vel.y: " << pelota[0]->getVel().y << std::endl;
		}

	}

	void hacks()
	{
		if (IsKeyPressed(KEY_Q))
		{
			gameOver = !gameOver;
		}

		if (IsKeyPressed(KEY_SPACE))
		{   // ----- reinicio ------
			init();
		}

		if (IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
		{
			for (int i = 0; i < maxPelotas; i++)
			{
				if (COLLISION_MANAGER::chequeoPuntoCirculo(mouse, pelota[i]->getCir(), pelota[i]->getRadio()))
				{
					pelota[i]->setCir(mouse);
				}
			}
		}
	}

	bool algunaPelotaEnMovimiento()
	{
		for (int i = 0; i < maxPelotas; i++)
		{
			if (pelota[i]->getEnMovimiento())
			{
				return false;
			}
		}
		if (!pelota[0]->getActivo())
		{
			pelota[0]->setActivo(true);
			pelota[0]->setCir({ posicionInicialBlanca.x, posicionInicialBlanca.y });
			pelota[0]->setRadio(radioGral);
			std::cout << "Pelota Blanca Spawneada\n";
		}
		return true;
	}
}
}