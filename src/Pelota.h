#pragma once
#include "raylib.h"
#include "GameManager.h"

namespace POOL {
	namespace PELOTA {

		const int radioGral = 20;
		const float PerdidaPotencia = 0.994f;
		const int maxPelotas = 16;

		const int disminucionPorcentual = 15;
		const float maxVelPelota = 10.0f;

		const float raizDe3 = 1.732050f;
		const float raizDe3PorRadio = raizDe3 * radioGral;
		const int separacionBolas = 2;
		const float animacionDesaparicion = 0.9f;
		const int separacionPelotasDibujado = 26;

		const Vector2 posicionInicialBlanca = { GAME_MANAGER::screenWidth * 2 / 10,  GAME_MANAGER::screenHeight * 5 / 10 };
		const Vector2 posicionInicialUno = { GAME_MANAGER::screenWidth * 6 / 10,  GAME_MANAGER::screenHeight * 5 / 10 };

		enum class TipoPelota
		{
			BLANCA,
			LISA,
			RALLADA,
			NEGRA
		};

		class Pelota
		{
		public:
			Pelota();
			~Pelota();

			void mover();
			void dibujar();
			void dibujar(int numero);

			bool getActivo();
			void setActivo(bool activo);
			TipoPelota getTipo();
			void setTipo(TipoPelota tipo);
			Color getColor();
			void setColor(Color color);
			Vector2 getCir();
			void setCir(Vector2 cir);
			int getRadio();
			void setRadio(int radio);
			Vector2 getVel();
			void setVel(Vector2 vel);
			float getMasa();
			void setMasa(float masa);
			float getVelDesgaste();
			void setVelDesgaste(float velDesgaste);
			bool getEnMovimiento();
			void setEnMovimiento(bool enMovimiento);
			bool getDesaparicion();
			void setDesaparicion(bool desaparicion);

		private:
			bool activo;
			TipoPelota tipo;
			Color color;
			Vector2 cir;
			int radio;
			Vector2 vel;
			float masa;
			float velDesgaste;
			bool enMovimiento;
			bool desaparicion;
		};
		extern Pelota* pelota[maxPelotas];

		void init();
		void update();
		void draw();
		void deInit();
	}
}
