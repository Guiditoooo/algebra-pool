#include "Borde.h"

namespace POOL {
	namespace BORDE {

		BORDE::Borde* borde[maxBordes];

		void deInit()
		{
			for (int i = 0; i < maxBordes; i++)
			{
				if (borde[i] != NULL)
				{
					delete borde[i];
					borde[i] = NULL;
				}
			}
		}

		Borde::Borde()
		{
			this->tipo = BordeUbicacion();
			this->rec = Rectangle();

			for (int i = 0; i < maxEsquinas; i++)
			{
				esq[i] = NULL;
			}
		}

		Borde::Borde(BordeUbicacion tipo, Rectangle rec)
		{
			this->tipo = tipo;
			this->rec = rec;

			for (int i = 0; i < maxEsquinas; i++)
			{
				esq[i] = new ESQUINA::Esquina();
			}
		}

		Borde::~Borde()
		{
			for (int i = 0; i < maxEsquinas; i++)
			{
				if (esq[i] != NULL)
				{
					delete esq[i];
					esq[i] = NULL;
				}
			}
		}

		void Borde::dibujar()
		{
			DrawRectangleLinesEx(rec, 1, BLACK);
		}

		BordeUbicacion Borde::getTipo()
		{
			return this->tipo;
		}

		void Borde::setTipo(BordeUbicacion tipo)
		{
			this->tipo = tipo;
		}

		Rectangle Borde::getRec()
		{
			return this->rec;
		}

		void Borde::setRec(Rectangle rec)
		{
			this->rec = rec;
		}
	}
}